package com.experiment.authclient.controller;

import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@RestController
public class TestController {

    @GetMapping("/hi")
    public ResponseEntity hi(){

        return new ResponseEntity<>("Hi", HttpStatus.OK);
    }


}
